return {
  'lukas-reineke/headlines.nvim',
  ft = { 'markdown', 'norg', 'rmd', 'org' },
  config = function()
    local opts = {}
    for _, ft in ipairs({ 'markdown', 'norg', 'rmd', 'org' }) do
      opts[ft] = {
        headline_highlights = {},
        bullets = {},  -- disabled bullets due to known issue
      }
      for i = 1, 6 do
        local hl = 'Headline' .. i
        vim.api.nvim_set_hl(0, hl, { link = 'Headline', default = true })
        table.insert(opts[ft].headline_highlights, hl)
      end
    end
    -- Setup headlines with a delay to prevent slowdown at file opening
    vim.schedule(function()
      require('headlines').setup(opts)
      require('headlines').refresh()
    end)
  end
}

